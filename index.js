const textSelection = document.getElementById("text-selection");
const backgroundSelection = document.getElementById("background-selection");
const linkSelection = document.getElementById("link-selection");
const body = document.body;

textSelection.addEventListener("change", function (e) {
  const labels = document.getElementsByTagName("label");
  for (let i = 0; i < labels.length; i++) {
    labels[i].style.color = e.target.value;
  }
});

backgroundSelection.addEventListener("change", function (e) {
  body.style.backgroundColor = e.target.value;
});

linkSelection.addEventListener("change", function (e) {
  const goHere = document.getElementById("go-here");
  goHere.style.color = e.target.value;
});
